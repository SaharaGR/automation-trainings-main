import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Ex4 {
	
	public static void Form(WebDriver driver) throws IOException{
		
		Xls_Reader reader = new Xls_Reader("./src/Ex5.xlsx");
		String sheetName = "Hoja1";
		
		String firstName = reader.getCellData(sheetName, 1, 1);
		String laststName = reader.getCellData(sheetName, 1, 2);
		String jobTitle = reader.getCellData(sheetName, 1, 3);
		String levelEducation = reader.getCellData(sheetName, 1, 4);
		String sex = reader.getCellData(sheetName, 1, 5);
		String yearsExp = reader.getCellData(sheetName, 1, 6);
		String date = reader.getCellData(sheetName, 1, 7);
		
		driver.get("http://formy-project.herokuapp.com/form");
		driver.findElement(By.id("first-name")).sendKeys(firstName);
		driver.findElement(By.id("last-name")).sendKeys(laststName);
		driver.findElement(By.id("job-title")).sendKeys(jobTitle);
		
		//driver.findElement(By.id("radio-button-2")).click();
		if (levelEducation == "High School") {
			driver.findElement(By.id("radio-button-1")).click();
		} 
		else if(levelEducation == "College") {
			driver.findElement(By.id("radio-button-2")).click();
		} 
		else {
			driver.findElement(By.id("radio-button-3")).click();
		}
		
		//driver.findElement(By.id("checkbox-2")).click();
		if (sex == "Male") {
			driver.findElement(By.id("checkbox-1")).click();
		} 
		else if(sex == "Female") {
			driver.findElement(By.id("checkbox-2")).click();
		} 
		else {
			driver.findElement(By.id("checkbox-3")).click();
		}
///////////////////////////////////////////////////////////////////////////////////////////////	
		File ss1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ss1, new File("C:\\Automation\\src\\ss1.png"));
		
		driver.findElement(By.id("select-menu")).click();
		
		
		if (yearsExp == "0" || yearsExp == "1") {
			driver.findElement(By.xpath("//option[2]")).click();
		} 
		else if(yearsExp == "2" || yearsExp == "3" || yearsExp == "4") {
			driver.findElement(By.xpath("//option[3]")).click();
		} 
		else if(yearsExp == "5" || yearsExp == "6" || yearsExp == "7"|| yearsExp == "8" || yearsExp == "9") {
			driver.findElement(By.xpath("//option[4]")).click();
		}
		else {
			driver.findElement(By.xpath("//option[5]")).click();
		}
		
		File ss2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ss2, new File("C:\\Automation\\src\\ss2.png"));
		
		driver.findElement(By.id("datepicker")).click();
		driver.findElement(By.className("today")).click();
		File ss3 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ss3, new File("C:\\Automation\\src\\ss3.png"));
		driver.findElement(By.cssSelector("a[href='/thanks']")).click();
		

		
		
		
}

}
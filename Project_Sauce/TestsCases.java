package Sahara;

import org.testng.annotations.Test;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

public class TestsCases extends Base{

	@Test
	public void testCase1() throws IOException{
		driver = initializeDriver();	
		Cases form = new Cases();
		form.Case1(driver);
	}
	
	@Test
	public void testCase2() throws IOException{
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case2(driver);
	}
	
	@Test
	public void testCase3() throws IOException{
		
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case3(driver);
	}
	
	@Test
	public void testCase4() throws IOException{	
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case4(driver);
	}
	
	@Test
	public void testCase5() throws IOException{		
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case5(driver);
	}
	
	@Test
	public void testCase6() throws IOException{
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case6(driver);
	}

	@Test
	public void testCase7() throws IOException{		
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case7(driver);
	}
	
	@Test
	public void testCase8() throws IOException{
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case8(driver);
	}
	
	@Test
	public void testCase9() throws IOException{
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case9(driver);
	}
	
	@Test
	public void testCase10() throws IOException{
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case10(driver);
	}
	
	@Test
	public void testCase11() throws IOException{
		driver = initializeDriver();
		Cases form = new Cases();
		form.Case11(driver);
	}	
	
}




package Sahara;

import org.apache.logging.log4j.*;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotSame;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Cases {
	
	private static Logger log = LogManager.getLogger(Cases.class.getName());
	
	public String[] ReadExcel(int num) throws IOException {
		Xls_Reader reader = new Xls_Reader("./src/Data.xlsx");
		String sheetName = "Data";

		String firstName = reader.getCellData(sheetName, 2, 2);
		String lastName = reader.getCellData(sheetName, 3, 2);
		String zip = reader.getCellData(sheetName, 4, 2);
		String usernames = reader.getCellData(sheetName, 0, num);
		String password = reader.getCellData(sheetName, 1, 2);

		String[] data = { firstName, lastName, zip, usernames, password };
		return data;
	}

	public static void YourInformation(WebDriver driver, String data[], String photos) throws IOException {

		driver.findElement(By.id("first-name")).sendKeys(data[0]);
		driver.findElement(By.id("last-name")).sendKeys(data[1]);
		driver.findElement(By.id("postal-code")).sendKeys(data[2]);
		Cases.TakeScreenshot(driver, photos);
		driver.findElement(By.id("continue")).click();
	}
	
	public static void TakeScreenshot(WebDriver driver, String photos) throws IOException {
		File ss2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ss2, new File("C:\\Users\\acersara\\Desktop\\ProjectSauce\\src\\" + photos));
	}
//-----------------------------Test Scenario 1-----------------------------------------------------------------------		
	public void Case1(WebDriver driver) throws IOException {
		//GET DATA FROM EXCEL FILE
		int num = 2;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC1_01_Login.png", "TC1_02_InventoryPage.png"};
		
		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]);
		Cases.TakeScreenshot(driver, photos[0]);
		driver.findElement(By.id("login-button")).click();
		Cases.TakeScreenshot(driver, photos[1]);
		assertEquals(driver.findElement(By.xpath("//span[@class='title']")).getText(), "PRODUCTS");
		System.out.println("Successfully logged in - The Test Case TC_001 was successful");
		//log.info("Successfully logged in - The Test Case TC_001 was successful");
		driver.close();
	}
	
	public void Case2(WebDriver driver) throws IOException {
		//GET DATA FROM EXCEL FILE
		int num = 2;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC2_01_Login.png"};
		
		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]+"x");
		driver.findElement(By.id("password")).sendKeys(data[4]);
		driver.findElement(By.id("login-button")).click();
		Cases.TakeScreenshot(driver, photos[0]);
		assertEquals(driver.findElement(By.cssSelector("h3[data-test='error']")).getText(), "Epic sadface: Username and password do not match any user in this service");
		log.error("Error when logging in");
		System.out.println("The Test Case TC_002 was successful");
		driver.close();
	}
	
	public void Case3(WebDriver driver) throws IOException {
		//GET DATA FROM EXCEL FILE
		int num = 2;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC3_01_Login.png"};
		
		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]+"x");
		driver.findElement(By.id("login-button")).click();
		Cases.TakeScreenshot(driver, photos[0]);
		assertEquals(driver.findElement(By.cssSelector("h3[data-test='error']")).getText(), "Epic sadface: Username and password do not match any user in this service");
		log.error("Error when logging in");
		System.out.println("The Test Case TC_003 was successful");
		driver.close();
	}
		
	public void Case4(WebDriver driver) throws IOException {

		int num = 3;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC4_01_Login.png" };

		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]);
		driver.findElement(By.id("login-button")).click();
		Cases.TakeScreenshot(driver, photos[0]);
		assertEquals(driver.findElement(By.xpath("//h3[contains(text(),'Epic sadface: Sorry, this user has been locked out')]")).getText(), "Epic sadface: Sorry, this user has been locked out.");
		System.out.println("The Test Case TC_004 was successful");
		log.error("Error when logging in");
		driver.close();
	}
	
	public void Case5(WebDriver driver) throws IOException {
		//GET DATA FROM EXCEL FILE
		int num = 4;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC1_05_Login.png", "TC1_05_InventoryPage.png"};
		
		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]);
		Cases.TakeScreenshot(driver, photos[0]);
		driver.findElement(By.id("login-button")).click();
		Cases.TakeScreenshot(driver, photos[1]);
		assertEquals(driver.findElement(By.xpath("//span[@class='title']")).getText(), "PRODUCTS");
		System.out.println("Successfully logged in - The Test Case TC_005 was successful");
		driver.close();
	}
	
	public void Case6(WebDriver driver) throws IOException {
		//GET DATA FROM EXCEL FILE
		int num = 5;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC6_01_Login.png", "TC1_06_InventoryPage.png"};
		
		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]);
		Cases.TakeScreenshot(driver, photos[0]);
		driver.findElement(By.id("login-button")).click();
		Cases.TakeScreenshot(driver, photos[1]);
		assertEquals(driver.findElement(By.xpath("//span[@class='title']")).getText(), "PRODUCTS");
		System.out.println("Successfully logged in - The Test Case TC_006 was successful");
		driver.close();
	}
	
	
	
//-----------------------------Test Scenario 2-----------------------------------------------------------------------	
	public void Case7(WebDriver driver) throws IOException {
		//GET DATA FROM EXCEL FILE
		int num = 2;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC7_01_Login.png", "TC7_02_Products.png", "TC7_03_YourInformation.png", "TC7_04_Overview.png", "TC7_05_Complete.png" };
		
		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]);
		Cases.TakeScreenshot(driver, photos[0]);
		driver.findElement(By.id("login-button")).click();
		
		// PRODUCTS
		driver.findElement(By.id("add-to-cart-sauce-labs-backpack")).click();
		driver.findElement(By.xpath("//a[@class='shopping_cart_link']")).click();
		Cases.TakeScreenshot(driver, photos[1]);
		driver.findElement(By.id("checkout")).click();
		
		// CHECKOUT: YOUR INFORMATION
		Cases.YourInformation(driver, data, photos[2]);
		
		// CHECKOUT: OVERVIEW
		Cases.TakeScreenshot(driver, photos[3]);
		driver.findElement(By.id("finish")).click();
		
		// CHECKOUT: COMPLETE!
		// WebDriverWait wait = new WebDriverWait(driver,1000);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
		assertEquals(driver.findElement(By.xpath("//h2[normalize-space()='THANK YOU FOR YOUR ORDER']")).getText(),"THANK YOU FOR YOUR ORDER");
		System.out.println("The Test Case TC_001 was successful");
		Cases.TakeScreenshot(driver, photos[4]);
		System.out.println("Successful purchase- The Test Case TC_007 was successful");
		driver.close();

	}

	public void Case8(WebDriver driver) throws IOException {

		int num = 2;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC8_01_Login.png", "TC8_02_Products.png", "TC8_03_YourInformation.png", "TC8_04_Overview.png", "TC8_05_Complete.png" };

		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]);
		Cases.TakeScreenshot(driver, photos[0]);
		driver.findElement(By.id("login-button")).click();

		// PRODUCTS
		
		String productName1 = driver.findElement(By.xpath("//div[normalize-space()='Sauce Labs Backpack']")).getText();
		String productPrice1 = driver.findElement(By.xpath("//div[@class='inventory_list']//div[1]//div[2]//div[2]//div[1]")).getText();
		String productName2 = driver.findElement(By.xpath("//div[normalize-space()='Sauce Labs Bike Light']")).getText();
		String productPrice2 = driver.findElement(By.xpath("//div[@id='inventory_container']//div[2]//div[2]//div[2]//div[1]")).getText();
		driver.findElement(By.xpath("//div[normalize-space()='Sauce Labs Backpack']")).click();
		assertEquals(driver.findElement(By.xpath("//div[@class='inventory_details_name large_size']")).getText(),productName1);
		assertEquals(driver.findElement(By.xpath("//div[@class='inventory_details_price']")).getText(),productPrice1);
		driver.findElement(By.xpath("//button[@id='add-to-cart-sauce-labs-backpack']")).click();
		driver.findElement(By.xpath("//button[@id='back-to-products']")).click();
		
		
		driver.findElement(By.xpath("//div[normalize-space()='Sauce Labs Bike Light']")).click();
		assertEquals(driver.findElement(By.xpath("//div[@class='inventory_details_name large_size']")).getText(),productName2);
		assertEquals(driver.findElement(By.xpath("//div[@class='inventory_details_price']")).getText(),productPrice2);
		driver.findElement(By.xpath("//button[@id='add-to-cart-sauce-labs-bike-light']")).click();
		driver.findElement(By.xpath("//a[@class='shopping_cart_link']")).click();
		Cases.TakeScreenshot(driver, photos[1]);
		driver.findElement(By.id("checkout")).click();

		// CHECKOUT: YOUR INFORMATION
		Cases.YourInformation(driver, data, photos[2]);

		// CHECKOUT: OVERVIEW
		Cases.TakeScreenshot(driver, photos[3]);
		driver.findElement(By.id("finish")).click();

		// CHECKOUT: COMPLETE!

		// WebDriverWait wait = new WebDriverWait(driver,100);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[normalize-space()='THANK
		// YOU FOR YOUR ORDER'")));
		assertEquals(driver.findElement(By.xpath("//h2[normalize-space()='THANK YOU FOR YOUR ORDER']")).getText(),
				"THANK YOU FOR YOUR ORDER");
		Cases.TakeScreenshot(driver, photos[4]);
		System.out.println("Successful purchase- The Test Case TC_008 was successful");
		driver.close();

	}

	public void Case9(WebDriver driver) throws IOException {

		int num = 2;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC9_01_Login.png", "TC9_02_Products.png", "TC9_03_YourInformation.png", "TC9_04_Overview.png", "TC9_05_Complete.png" };

		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]);
		Cases.TakeScreenshot(driver, photos[0]);
		driver.findElement(By.id("login-button")).click();

		// PRODUCTS
		driver.findElement(By.id("add-to-cart-sauce-labs-backpack")).click();
		driver.findElement(By.id("add-to-cart-sauce-labs-bike-light")).click();
		driver.findElement(By.id("add-to-cart-sauce-labs-bolt-t-shirt")).click();
		driver.findElement(By.xpath("//div[normalize-space()='Sauce Labs Fleece Jacket']")).click();
		driver.findElement(By.id("add-to-cart-sauce-labs-fleece-jacket")).click();
		driver.findElement(By.xpath("//a[@class='shopping_cart_link']")).click();
		driver.findElement(By.id("remove-sauce-labs-backpack")).click();
		Cases.TakeScreenshot(driver, photos[1]);
		driver.findElement(By.id("checkout")).click();

		// CHECKOUT: YOUR INFORMATION
		Cases.YourInformation(driver, data, photos[2]);

		// CHECKOUT: OVERVIEW
		Cases.TakeScreenshot(driver, photos[3]);
		driver.findElement(By.id("finish")).click();

		// CHECKOUT: COMPLETE!

		// WebDriverWait wait = new WebDriverWait(driver,100);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[normalize-space()='THANK
		// YOU FOR YOUR ORDER'")));
		assertEquals(driver.findElement(By.xpath("//h2[normalize-space()='THANK YOU FOR YOUR ORDER']")).getText(),
				"THANK YOU FOR YOUR ORDER");
		System.out.println("The Test Case TC_002 was successful");
		Cases.TakeScreenshot(driver, photos[4]);
		System.out.println("Successful purchase- The Test Case TC_009 was successful");
		driver.close();

	}

	public void Case10(WebDriver driver) throws IOException {

		int num = 4;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC10_01_Login.png", "TC10_02_Products.png", "TC10_03_YourInformation.png", "TC10_04_YourInformation.png",  };

		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]);
		Cases.TakeScreenshot(driver, photos[0]);
		driver.findElement(By.id("login-button")).click();

		// PRODUCTS
		String productName1 = driver.findElement(By.xpath("//div[normalize-space()='Sauce Labs Backpack']")).getText();
		String productPrice1 = driver.findElement(By.xpath("//div[@class='inventory_list']//div[1]//div[2]//div[2]//div[1]")).getText();
		driver.findElement(By.xpath("//div[normalize-space()='Sauce Labs Backpack']")).click();
		assertNotSame(driver.findElement(By.xpath("//div[@class='inventory_details_name large_size']")).getText(),productName1);
		log.error("Error in the name of the products");
		assertNotSame(driver.findElement(By.xpath("//div[@class='inventory_details_price']")).getText(),productPrice1);
		log.error("Error in the price of the products");
		driver.findElement(By.xpath("//button[@id='add-to-cart-sauce-labs-fleece-jacket']")).click();
		
		driver.findElement(By.xpath("//a[@class='shopping_cart_link']")).click();
		Cases.TakeScreenshot(driver, photos[1]);
		driver.findElement(By.id("checkout")).click();

		// CHECKOUT: YOUR INFORMATION
		Cases.YourInformation(driver, data, photos[2]);
		Cases.TakeScreenshot(driver, photos[3]);
		assertEquals(driver.findElement(By.xpath("//h3[normalize-space()='Error: Last Name is required']")).getText(), "Error: Last Name is required");
		log.fatal("Error entering personal information");
		System.out.println("The Test Case TC_0010 was successful");
		driver.close();
		
	}

	public void Case11(WebDriver driver) throws IOException {

		int num = 5;
		String[] data = this.ReadExcel(num);
		String[] photos = { "TC5_01_Login.png", "TC5_02_Products.png", "TC5_03_YourInformation.png", "TC5_04_Overview.png", "TC5_05_Complete.png" };

		// LOGIN
		driver.findElement(By.id("user-name")).sendKeys(data[3]);
		driver.findElement(By.id("password")).sendKeys(data[4]);
		Cases.TakeScreenshot(driver, photos[0]);
		driver.findElement(By.id("login-button")).click();

		// PRODUCTS
		driver.findElement(By.id("add-to-cart-sauce-labs-bike-light")).click();
		driver.findElement(By.xpath("//a[@class='shopping_cart_link']")).click();
		Cases.TakeScreenshot(driver, photos[1]);
		driver.findElement(By.id("checkout")).click();

		// CHECKOUT: YOUR INFORMATION
		Cases.YourInformation(driver, data,photos[2]);

		// CHECKOUT: OVERVIEW
		Cases.TakeScreenshot(driver, photos[3]);
		driver.findElement(By.id("finish")).click();

		// CHECKOUT: COMPLETE!

		// WebDriverWait wait = new WebDriverWait(driver,100);
		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[normalize-space()='THANK
		// YOU FOR YOUR ORDER'")));
		Cases.TakeScreenshot(driver, photos[4]);
		assertEquals(driver.findElement(By.xpath("//h2[normalize-space()='THANK YOU FOR YOUR ORDER']")).getText(), "THANK YOU FOR YOUR ORDER");
		System.out.println("The Test Case TC_0010 was successful");
		driver.close();

	}

}

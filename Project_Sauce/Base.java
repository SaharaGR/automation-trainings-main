package Sahara;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {

	public WebDriver driver;

	public WebDriver initializeDriver() {

		System.setProperty("webdriver.chrome.driver", "C:\\Users\\acersara\\Desktop\\Carrera\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.saucedemo.com/");
		// driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		return driver;

	}


}

import java.util.Scanner;

public class Assignment_1_4 {
	
	static Scanner in = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Write str1: ");
		String str1 = in.nextLine();
		System.out.println("Write str2: ");
		String str2 = in.nextLine();
		System.out.println(str1 + str2);
		
		
		
		System.out.println("Write number Fibonacci: ");
		String x = in.nextLine();
		int num = Integer.parseInt(x);
		numFibo(num);
		
		
		System.out.println("Write number 1: ");
		String number1 = in.nextLine();
		int num1 = Integer.parseInt(number1);
		System.out.println("Write number 2: ");
		String number2 = in.nextLine();
		int num2 = Integer.parseInt(number2);
		System.out.println("Write number 3: ");
		String number3 = in.nextLine();
		int num3 = Integer.parseInt(number3);
		System.out.println("Write number 4: ");
		String number4 = in.nextLine();
		int num4 = Integer.parseInt(number4);
		int max = getGratestNum(num1, num2, num3, num4);
		System.out.println("The greatest number is: " + max);	
	}
	
	public static void numFibo(int num) {
		int  num1 = 0, num2 = 1, suma = 1; 
        for (int i = 1; i <= num; i++) {
            suma = num1 + num2;
            System.out.println(suma);
            num1 = num2;
            num2 = suma;
        }
	}
	
	
	public static int getGratestNum(int num1, int num2, int num3) {
		int num = Math.max(num1,Math.max(num2,num3));
        return num;
	}
	
	public static int getGratestNum(int num1, int num2, int num3, int num4) {
		int max = Math.max(Math.max(num1,num2),Math.max(num3,num4));
		return max;
	}
}


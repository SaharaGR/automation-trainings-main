import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Form {
	
	public static void main(String[] args) throws InterruptedException{
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://formy-project.herokuapp.com/form");
		
		driver.findElement(By.id("first-name")).sendKeys("Sahara");
		driver.findElement(By.id("last-name")).sendKeys("Guerrero");
		driver.findElement(By.id("job-title")).sendKeys("Trainee");
		driver.findElement(By.id("radio-button-2")).click();
		driver.findElement(By.id("checkbox-2")).click();
		
		driver.findElement(By.id("select-menu")).click();
		driver.findElement(By.xpath("//option[2]")).click();
		
		driver.findElement(By.id("datepicker")).click();
		driver.findElement(By.className("today")).click();
		
		driver.findElement(By.cssSelector("a[href='/thanks']")).click();
		
}

}
